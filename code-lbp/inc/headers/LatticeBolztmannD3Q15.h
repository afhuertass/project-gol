#include <arrayfire.h>
// armazon de la clase encargada de las simulaciones

class LatticeBolztmannD3Q15{
 private:
  const int q = 15;
  int Lx,Ly,Lz; // variables del lattice
  float V[3][q]; // velocidades del lattice
  array f; // funciones de equilibrio
  array Vel_x, Vel_y , Vel_z ;
  array w; // funciones de peso
 public:
  LatticeBolztmannD3Q15(int Lx, int Ly, int Lz); // constructor - reciba el tamaño del lattice
  void Inicie(float r0, float Ux0, float Uy0, float Uz0); // reciba la densidad y velocidades cero
  
  array feq( array &rhos, array &Uxs , array &Uys , array &Uzs);
  
  array rho();
  array Jx();
  array Jy();
  array Jz();
  
  void Colission(void);
  void Adveccion(void);
  
};
