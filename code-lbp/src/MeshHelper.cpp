#include <inc/meshIncludes.h> 

#include <inc/headers/MeshHelper.h>

using namespace CGAL::parameters;

MeshHelper::MeshHelper(Polyhedron_m p){
  // en el constructor generamos el mesh
  this->generateMesh(p);
  
    
}
void MeshHelper::generateMesh(Polyhedron_m p){
  Mesh_domain domain(p);
  Mesh_criteria criteria(facet_angle=25, facet_size=0.15, facet_distance=0.008,cell_radius_edge_ratio=3);
  // criterios, para ensayos, probamos con estos y luego extendemos.

  this->mesh = CGAL::make_mesh_3<C3t3>( domain , criteria , no_perturb(), no_exude()) ;
  

}
void MeshHelper::saveMeshFile(){
  
  std::ofstream medit_file("out.mesh");
  this->mesh.output_to_medit(medit_file);
  medit_file.close();

};
void MeshHelper::saveBoundary(){
  std::ofstream off_file("boundary.off");
  this->mesh.output_boundary_to_off(off_file);
  off_file.close();
  
}
bool MeshHelper::isPointIn(const Point  &query){
  // 
  Locate_type lt;
  int ii, jj;
  Tr &tr = mesh.triangulation();
  //Cell_handle infinite = tr.infite_cell();
  Cell_handle ch = tr.locate( query , lt , ii ,jj );
  if ( lt == Tr::OUTSIDE_CONVEX_HULL && !tr.is_infinite(ch)) {
    return false;
  }
  if(lt == Tr::CELL && !tr.is_infinite(ch)  ){
    return true;
  }else {
    return false;
  }
 
}
