/*#include "meshIncludes.h"

#include "polyhedronContainer.h"
#include "Modelo.h"
#include "meshHelper.h"
*/
// #include "classes.h"
#include <inc/meshIncludes.h>
#include <inc/headers/Modelo.h>
#include <inc/headers/MeshHelper.h>

int main(){

  Modelo casa("cubo.obj");
  Polyhedron_m p = casa.createPolyForMesh();
  MeshHelper mh( p );
  mh.saveMeshFile();
  Point punto(0,0,0);
  int size = 100;
  int grid[size][size][size]; // grilla de simulacion 50x50x50 celdas
  // digamos que el cubo esta en un espacio de 4x4x4 metros
  // intervalo -2 hasta 2  
  // 1 celda = 0.04 metros
  double x0, y0, z0; // conejo -7e-2  7e-2
  double ini = -2, fini = 2;
  x0 = y0 = z0 = ini;
  double deltaM = (fini-ini)/size; 
  for( int ix = 0 ; ix < size ; ix++ ,x0 +=deltaM){
    for(int iy = 0 ; iy < size ; iy++, y0 += deltaM){
      for(int iz = 0; iz < size ; iz++ , z0+= deltaM){
	Point punto(x0,y0,z0);
	//std::cout << x0 << " " << y0  <<" " << z0  <<  std::endl;
	if ( mh.isPointIn(punto)){
	  grid[ix][iy][iz] = 121;
	  std::cout << x0 << " " << y0  <<" " << z0  << " " << grid[ix][iy][iz] << std::endl;
	}else {
	  grid[ix][iy][iz] = 0;
	}
	//std::cout << x0 << " " << y0  <<" " << z0  << " " << grid[ix][iy][iz] << std::endl;
      }
      z0 = ini;
    }
    y0 = ini;
    std::cout << std::endl;
  }
  for( int ix = 0 ; ix < 50 ; ix++){
    for(int iy = 0 ; iy < 50 ; iy++){ 
      for(int iz = 0; iz < 50 ; iz++){ 
	//std::cout << ix << " " << iy  <<" " << iz  << " " << grid[ix][iy][iz] << std::endl;
	
      } 
    
    }
    //std::cout << std::endl;
  }
  // if( mh.isPointIn(punto)) rr = 10;
  //std::cout << rr << std::endl;
  //mh.saveMeshFile();
  return 0;
}
